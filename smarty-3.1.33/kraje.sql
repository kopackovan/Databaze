-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pát 26. dub 2019, 08:28
-- Verze serveru: 5.6.15-log
-- Verze PHP: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `uzemnicelkycr`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `kraj`
--

CREATE TABLE IF NOT EXISTS `kraj` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Kraje',
  `kod` varchar(7) COLLATE utf8_czech_ci NOT NULL COMMENT 'Kód kraje',
  `nazev` varchar(80) COLLATE utf8_czech_ci NOT NULL COMMENT 'Název kraje',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Kraj' AUTO_INCREMENT=15 ;

--
-- Vypisuji data pro tabulku `kraj`
--

INSERT INTO `kraj` (`id`, `kod`, `nazev`) VALUES
(1, 'CZ041', 'Karlovarský kraj'),
(2, 'CZ020', 'Středočeský kraj'),
(3, 'CZ031', 'Jihočeský kraj'),
(4, 'CZ064', 'Jihomoravský kraj'),
(5, 'CZ052', 'Královéhradecký kraj'),
(6, 'CZ053', 'Pardubický kraj'),
(7, 'CZ080', 'Moravskoslezský kraj'),
(8, 'CZ051', 'Liberecký kraj'),
(9, 'CZ071', 'Olomoucký kraj'),
(10, 'CZ063', 'Kraj Vysočina'),
(11, 'CZ042', 'Ústecký kraj'),
(12, 'CZ072', 'Zlínský kraj'),
(13, 'CZ032', 'Plzeňský kraj'),
(14, 'CZ010', 'Hlavní město Praha');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
