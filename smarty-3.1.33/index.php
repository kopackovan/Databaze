<?php
include("libs/Smarty.class.php");
$smarty=new Smarty();

$dbname = "kraje";
$servername = "localhost";
$username = "root";
$password = "";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$conn->set_charset("utf8");
if(isset ($_GET["okres"])){
    $sql="SELECT * FROM obec WHERE kraj_id=" . $_GET["kraj"] . " AND okres_id=" . $_GET["okres"];
    $vysledek = $conn->query($sql);

    if ($vysledek->num_rows > 0) {
        while($row = $vysledek->fetch_assoc()) {
            $obce[]=array(
                'nazev' => $row["nazev"],
                'id' => $row["id"]
            );
        }
    }
    $smarty->assign("obce",$obce);

    $sql="SELECT * FROM okres WHERE kraj_id=" . $_GET["kraj"] . " AND id=" . $_GET["okres"];
    $vysledek = $conn->query($sql);

    if ($vysledek->num_rows > 0) {
        while($row = $vysledek->fetch_assoc()) {
            $okres = $row["nazev"];


        }
    }
    $smarty->assign("kraj_id", $_GET["kraj"]);
    $smarty->assign("okres",$okres);
    $smarty->display("okresy.tpl");
}

elseif(isset ($_GET["kraj"])){
    $sql="SELECT * FROM okres WHERE kraj_id=" . $_GET["kraj"];
    $vysledek = $conn->query($sql);

    if ($vysledek->num_rows > 0) {
        while($row = $vysledek->fetch_assoc()) {
            $okresy[]=array(
                'nazev' => $row["nazev"],
                'id' => $row["id"]
            );
        }
    }
    $smarty->assign("okresy",$okresy);

    $sql="SELECT * FROM kraj WHERE id=" . $_GET["kraj"];
    $vysledek = $conn->query($sql);

    if ($vysledek->num_rows > 0) {
        while($row = $vysledek->fetch_assoc()) {
            $kraj=array(
                'nazev' => $row["nazev"],
                'id' => $row["id"]
            );


        }
    }
    $smarty->assign("kraj",$kraj);
    $smarty->display("kraje.tpl");
}

else{

$smarty->display("index.tpl");


}
?>
